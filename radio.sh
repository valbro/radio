#!/usr/bin/env bash
apt-get upgrade
apt-get install mc
apt-get install htop
apt-get install vim
apt-get install git-core
apt-get install lighthttpd
apt-get install icecast2
cp icecast.xml /etc/icecast2/
cp icecast2 /etc/default/
cp denyip /home/
service lighthttpd start
service icecast2 start
apt-get install mpd
cp mpd.conf /etc/
mkdir /home/radio
mkdir /home/radio/Rotation
apt-get install libmp3lame0
apt-get install libmp3lame-dev
apt-get install libshout3
/etc/init.d/mpd start-create-db
apt-get install ncmpc
chmod "+x" script
./script
