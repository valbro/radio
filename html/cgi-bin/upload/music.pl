#!/usr/bin/perl -w

$basedir = "/home/fiztehradio/rotation/music/upload";
$donepage = "/cgi-bin/upload/done_music.sh";

use CGI;

$onnum = 0;
while ($onnum != 9) {
	
	my $req = new CGI; 
	my $file = $req->param("music$onnum"); 
	
	if ($file ne "") {
		my $fileName = $file; 
		$fileName =~ s!^.*(\\|\/)!!; 
		$newmain = $fileName;
		open (OUTFILE,">$basedir/$fileName");
		print "$basedir/$fileName";
		while (my $bytesread = read($file, my $buffer, 1024*20000)) {
			print OUTFILE $buffer;
		} 
	
	close (OUTFILE);

	}
	#else {
	#	print $req->header ( );
	#	print "There was a problem uploading your music (try a smaller file < 20mb).";
	#	exit;
	#}

	$onnum++;

}
print "Content-type: text/html\n";
print "Location:$donepage\n\n";
