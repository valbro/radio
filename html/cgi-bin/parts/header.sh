#!/bin/bash

echo "Content-type: text/html"
echo ''
echo "<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en\" lang=\"en\">"
echo "<head>"
echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />"
echo "<title>$1</title>"
echo "<link type=\"text/css\" rel=\"stylesheet\" href=\"/stylesheet.css\"/>"
echo "<link rel=\"shortcut icon\" href=\"/images/favicon.ico\"/>"
echo "</head>"
echo "<body>"
echo "<a href=\"/\"><img alt=\"Физтех.Радио\" src=\"/images/main.jpg\" width=\"150\" height=\"150\"></a>"
